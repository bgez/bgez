# Contributing to BGEz

## Setup

First, install `virtualenv` and `poetry`:

```sh
pip3 install --user virtualenv poetry
```

## 1. Working on BGEz sources

### 1.a. Environment Setup

Clone `bgez` sources:

```sh
git clone https://gitlab.com/bgez/bgez.git -b develop bgez
cd bgez
```

From there, you should create a virtualenv running Python >= 3.6.
I recommend create a virtualenv using UPBGE's bundled Python:

```sh
virtualenv -p /path/to/upbge/2.79/python/bin/python3.6m .venv
```

You can now start developing.

### 1.b. Running the Unit-tests

Once you add a feature, you might want to write tests for it, here is how to run the test suite:

```sh
poetry run python -m bgez.__test__
```

Test scripts should be named `<module>_test.py`.

We are using `unittest` for the time being.

## 2. Working on a project while developing BGEz

### 2.a. Environment Setup

```sh
cd your-project

# If not already initialized, do it
poetry init

# Setting up development Python virtualenv
virtualenv -p /path/to/upbge/2.79/python/bin/python3.6m .venv

# Cloning the sources locally and using them as current project dependency
git clone https://gitlab.com/bgez/bgez.git -b develop .bgez
poetry add bgez --path=.bgez
```

### 2.b. Running the Game

BGEz adds an utility scripts that configures application embedding a Python interpreter to see libraries inside a virtualenv: `bgez.launcher`.

```sh
poetry run bgez.launcher upbgeplayer ...args... game.blend
```

In the case of UPBGE, it will first see files directly next to the blend file, then it will look inside the virtualenv.

Keep this in mind and watch out for module name collisions in your workspace.
