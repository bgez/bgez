# Keymaps

```py
from bgez.logic.input import Keymap, ButtonType, AxisType

keymap = Keymap()

action_a = keymap.define('action_a', ButtonType, default = ['space', 'shift + w'])
action_b = keymap.define('action_b', AxisType, default=['mouselook.x'])
sub_action_b = keymap.define('sub_action_b.left', ButtonType)
sub_action_b = keymap.define('sub_action_b.right', ButtonType)

keymap.link('action_b', 'sub_action_b.left', lambda button: 1.0 if button.pressed else 0.0)
keymap.link('action_b', 'sub_action_b.right', lambda button: -1.0 if button.pressed else 0.0)
```

## Abstract

Keymapping is a pain. Hardcoding the input polling is not difficult, but it proves complicated to manage once you start trying to let the user define his own mappings. Then you might need to dump the keymap on the disk, and be able to easily go back and forth.

When on disk, all the information you will be able to store will be names. The simpliest format that is supported would be something like the following:

`json`:
```json
{
    "action_a": ["space", "shift + w"],
    "action_b": ["mousex"]
}
```

`yaml`:
```yml
action_a:
    - space
    - shift + w
action_b:
    - mousex
```

So all you will get back are strings/labels. You now need to know what label corresponds to a keyboard key, and which one represents an axis. And you need to know what can be mixed and what cannot (what should happen if you use an axis in a place where a button is expected?)

## Design

Pseudo-Python to illustrate the structures:

```py
# function to convert a value from one type to the other
def Converter(value: Any) -> Any: ...

# define the kind of value an input returns
class InputType:

    # how to mix values together (OR)
    @classmethod
    def union(values: List[Any]) -> Any: ...

    # how to merge values together (AND)
    @classmethod
    def intersection(values: List[Any]): Any: ...

# handle: return state for a specific device/key
class Input:
    type: InputType
    value: Any

# how to fetch a device input from a string
Registry = Dict[string, Input]

# define actions to regroup inputs
Keymap = Dict[string, KeymapEntry]

# handle: get the value and bindings for action
KeymapEntry = Input & List[string]
```

## Keymap Objects and Actions

A `Keymap` is simply your high level construct to manage inputs via actions.

What you do is define `actions` such as `jump`, `forward`, `backward` or `fire`, and specify the kind of input you expect to fetch from it. Is it a button? Is it an axis?

Once defined, you can add inputs to that action. An action in itself is just an abstract concept. You have to bind concrete inputs to it, such as keyboard keys or the mouse axis... Just make sure that the correct input type is bound to the action, you can even mix it if you describe how to convert the values correctly.

## Input Registry

A `Keymap` takes a registry as first argument. It can default to some already-cooked-but-simple builtin registry from the framework.

The role of a registry is to define the link between input names and their actual implementation. For instance, `registry['space']` should return the object that will allow you to check the state of the spacebar. But remember that this is an arbitrary mapping: you could make `space` map to the mouse left button if you were crazy enough.

The registry also allows you to support custom inputs that are not already defined in the framework. Just add an entry for a valid `Input` object, done.

## Input Objects

`Input` objects are small structures that allow you to fetch the value of something (keyboard, mouse, joystick, wiimote, whatever...)

They must also define a type that describes their output value. An input can only be of one type. For instance, a `KeyboardInput` would say that it respects the `ButtonType`, allowing it to interact with other inputs defined in such a way.

```py
input = registry['key']
input.value
input.type
```

`Input` objects also expose some utility methods/fields from their `InputType`. For instance, `ButtonType` inputs allow you to call `input.pressed`, which will call `input.type.pressed(input.value)`.

This allows any `Input` object returning a valid type to also benefit from utilities defined in the type itself.

## InputType Objects

`InputType` objects are used to discriminate incompatible inputs. What to do with an axis if we expected a button?

Fortunately enough, knowing the kind of value that some input will return allows us to eventually handle such cases where the types are not the same. When defining an action within a keymap, you can specify a mapping such as `type -> function` in order to accept different input types:

```py
def doSomething(value):
    return 0.3 * value

def doSomethingElse(value):
    return 1 if value else 0

action = keymap.define('action', ExpectedType, converters = {
    OtherType: doSomething,
    AnotherType: doSomethingElse,
})
```

Basically, you just have to provide conversion functions. They take the value in the wrong input space/type, and should return the new value in the expected input type (`ExpectedType` in this example).

This is useful if you want to mix inputs for some actions, such as character movement: Would be neet to use both keyboard and joysticks for that. In that case you could expect an `AxisType`, and convert the values of `ButtonType` to floating values to mimic joysticks. As simple as that.

## KeymapEntry Objects

When doing `keymap.define(...)`, you will be handed back a `KeymapEntry`.

These are input-like objects, meaning they expose a `type` and a `value`. The `value` is in fact the aggretated inputs of all the bindings for this specific entry, or action.

Say you do this:

```py
action = keymap.define('action', ButtonType, default = ['a', 'b', 'c + d'])

print(action.value)
```

Then `action.value` will return `action.type.union(a, b, action.type.intersection(c, d))`. This means that it will react to `a` or `b` or `c + d`, where `c + d` will require both `c` and `d` to be in the same state.
