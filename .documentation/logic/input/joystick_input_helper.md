# Joystick Input Helper

Values are taken from Paul Maréchal's personal computer using UPBGE 2.4.0 (Windows 10)

## PS3 Controller + DS3Tools XBOX Controller Emulator

| Index | Button | Code | Axis | Code |
|:-:|:-:|:-:|:-:|:-:|
| 0 | Cross | `button0` | Left-Stick X | `axis0` |
| 1 | Circle | `button1` | Left-Stick Y | `axis1` |
| 2 | Square | `button2` | Right-Stick X | `axis2` |
| 3 | Triangle | `button3` | Right-Stick Y | `axis3` |
| 4 | Select | `button4` | L2 | `axis4` |
| 5 | Start | `button6` | R2 | `axis5` |
| 6 | L3 | `button7` |
| 7 | R3 | `button8` |
| 8 | L1 | `button9` |
| 9 | R1 | `button10` |
| 10 | Up | `button11` |
| 11 | Down | `button12` |
| 12 | Left | `button13` |
| 13 | Right | `button14` |
