# `game.scenes`

## Abstract

This object manages the scenes in the runtime. You can access the scenes and add overlays from there.

## Methods

- `scenes[name]`

    Get a scene by its name.

    **Returns**: [`Scene`](./scene.md) object.

- *async* `scene.overlay(name)`

    Add a scene from name as an overlay.\
    Scene must be inactive. Can only be added once.

    **Returns**: [`Promise`](../asyn/promise.md)`<`[`Scene`](./scene.md)`>`

- *async* `scene.overlay(name)`

    Add a scene from name as an underlay.\
    Scene must be inactive. Can only be added once.

    **Returns**: [`Promise`](../asyn/promise.md)`<`[`Scene`](./scene.md)`>`
