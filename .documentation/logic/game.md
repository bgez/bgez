# `from bgez.logic import game`

## Abstract

`game` is an object exposing most of the global game API within BGEz.

## Events

- `'start'`

    Triggered when your game finished the import/initialisation phase and is about to run the event loop.

- `'error'`

    Triggered when and exception arised and stopped the main loop.

- `'quit'`

    Triggered when someone tries to quit the game by doing `game.quit()`.

- `'exit'`

    Triggered when the game engine is about to shutdown, you can still block the runtime. If someone kills your process the event might not get fired.

## Methods

- `game.on(event: str, callback)`

    Register a callback to the events triggered during runtime.

- `game.timeout(delay: float, callback, *args, **kwargs)`

    Register a callback to be run later in time with `args` and `kwargs`.\
    `delay` is in seconds.

- `game.interval(delay: float, callback, *args, **kargs)`

    Register a callback to be run peridically over time with `args` and `kwargs`.\
    `delay` is in seconds.

## Attributes

- `game.scenes`

    Returns a [`SceneManager`](./scenes.md).
