# `from bgez.logic import Scene`

## Events

- `'logic'`

    Triggered at each logic step (from inside the event loop).

- `'pre_draw_setup'`

    Triggered by the scene when at the pre-draw-setup phase.

- `'pre_draw'`

    Triggered by the scene when at the pre-draw phase.

- `'post_draw'`

    Triggered by the scene when at the post-draw phase.

- `'remove'`

    Triggered when the scene is removed from the runtime.

## Methods

- `scene.on(event: str, callback)`

## Attributes

- `scene.objects`

    Objects living in the runtime.

    **Returns**: List<[`GameObject`](./game_object.md)>

- `scene.inactives`

    Objects that are inactive (you can spawn them in the scene).

    **Returns**: List<[`GameObject`](./game_object.md)>

- `scene.cameras`

    Cameras living in the runtime.

    **Returns**: List<[`GameObject`](./game_object.md)>

- `scene.camera`

    Current active camera.

    **Returns**: [`GameObject`](./game_object.md)

- `scene.cameras`

    Cameras living in the runtime.

    **Returns**: List<[`GameObject`](./game_object.md)>
