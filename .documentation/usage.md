# BGEz Usage

When creating a project using BGEz, it is strongly advised to use virtualenvs.

Virtualenvs allow you to isolate dependencies, to avoid library version collisions: https://pypi.org/project/virtualenv/.

One issue with UPBGE/Blender is that they aren't meant to run in such virtualenvs, this is why BGEz packs a special launcher script meant to fix that issue: `bgez.launcher`.

If you did correctly setup your virtualenv, then the script will be accessible inside said virtualenv.

## Setup

This is the base setup that you should do for each workflow

```sh
# DO ONCE: Install virtualenv
pip3 install --user virtualenv

# Move inside your project's folder
cd your-project

# DO ONCE: Create a virtualenv to isolate dependencies
virtualenv -p /path/to/upbge/2.79/python/bin/python3.6m .venv
```

## 1. Minimal Workflow

```sh
# Activate the virtualenv
.venv/bin/activate

# DO ONCE: Install bgez as dependency
pip install bgez

# To run your game
bgez.launcher upbgeplayer ...args... game.blend

# Run UPBGE editor
poetry run bgez.launcher upbge
```

## 2. Poetry Workflow

```sh
# DO ONCE: Install Poetry
pip3 install --user poetry

# DO ONCE:
poetry add bgez

# To run your game
poetry run bgez.launcher upbgeplayer ...args... game.blend

# Run UPBGE editor
poetry run bgez.launcher upbge
```

You can also get a shell inside the virtualenv by doing

```sh
poetry shell
```
