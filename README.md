# BGEz Framework

## What?
The BGEz framework is designed to provide a new API for the Blender Game Engine. It still uses the standard API, but the goal is to provide ways to write your game in Python in a maintenable manner.

## Why?
This project is based on the conviction that unmaintainable projects are doomed to fail. With the way things currently are on the BGE, making something more or equally big as a medium-sized project is almost impossible to manage. Teamworking is inefficient. BGEz is meant to fix that.

## Resources
- [Usage](.documentation/usage.md)
- [Development](.documentation/development.md)
