from unittest import TestCase, skip
from threading import Lock

from bgez.asyn.task_manager import TaskManager, UNRESOLVED, Full

DONE = object()
MAX_TIMEOUT = 3

POOL_SIZE = 2
MAX_TASKS_QUEUED = 3

class TaskManagerTest(TestCase):

    def setUp(self):
        self.tm = TaskManager(POOL_SIZE, MAX_TASKS_QUEUED)
        self.callback_called = 0
        self.values = {}
        self.locks = {}

    def tearDown(self):
        if not self.tm.closed:
            self.tm.close()

    def test_add_task(self):
        values = 1, 2, 3

        for value in values:
            lock = Lock()
            lock.acquire()

            handle = self.tm.add_task(self._task, value, lock, callback=self._done_callback)

            self.values[handle] = value
            self.locks[handle] = lock

        for handle in self.values.keys():
            self.assertEqual(handle.value, UNRESOLVED)

        for lock in self.locks.values():
            lock.release()

        for handle in self.values.keys():
            expected = self.values[handle]
            value = handle.wait_value(MAX_TIMEOUT)
            self.assertEqual(value, expected)

        self.tm.close(MAX_TIMEOUT)
        self.tm()
        self.assertEqual(self.callback_called, len(values))

    def test_full_exception(self):
        exceptions_amount = 0

        for _ in range(MAX_TASKS_QUEUED + POOL_SIZE):
            try:
                self.tm.add_task(self._task)
            except Full:
                exceptions_amount += 1

        self.assertTrue(exceptions_amount > 0)

    def test_task_exception(self):

        def exception_task(exception):
            raise exception("Test raised exception")

        exceptions = Exception, ValueError, NotImplementedError
        exceptions_amount = 0
        handles = []

        for exception in exceptions:
            handles.append(self.tm.add_task(exception_task, exception))

        for i, handle in enumerate(handles):
            try:
                self.tm()
                result = handle.wait_value()
            except exceptions[i]:
                exceptions_amount += 1

        self.assertEqual(exceptions_amount, len(exceptions))

    def test_deco_as_task(self):

        @self.tm.as_task(self._done_callback)
        def sleepy_deco(lock, value):
            with lock:
                return value

        values = 1, 2, 3

        for value in values:
            lock = Lock()
            handle = sleepy_deco(lock, value)
            self.values[handle] = value

        for handle in self.values.keys():
            try:
                value = handle.wait_value(1)
            except TimeoutError:
                return self.fail()

            self.assertEqual(value, self.values[handle])

        self.tm.close(MAX_TIMEOUT)
        self.tm()
        self.assertEqual(self.callback_called, len(values))

    def test_available_workers(self):

        lock = Lock()
        with lock:
            handle = self.tm.add_task(self._task, lock=lock)
            self.assertEqual(self.tm.available_workers, POOL_SIZE - 1)

        handle.wait_value(3)
        self.assertEqual(self.tm.available_workers, POOL_SIZE)

    def test_init(self):
        self.assertEqual(self.tm._task_queue.maxsize, MAX_TASKS_QUEUED + POOL_SIZE)
        self.assertEqual(len(self.tm._workers), POOL_SIZE)

    def test_close_success(self):
        self.assertTrue(self.tm.close(MAX_TIMEOUT))

    def test_close_fail(self):
        lock = Lock()
        with lock:
            self.tm.add_task(self._task, DONE, lock)
            self.assertFalse(self.tm.close(.1))


    def _task(self, value = DONE, lock = None):
        if lock is None:
            return value
        with lock:
            return value

    def _done_callback(self, handle):
        self.callback_called += 1
