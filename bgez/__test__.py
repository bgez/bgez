import unittest

def find():
    import os.path
    import bgez

    from bgez import logger
    logger.setLevel("CRITICAL")

    class CustomLoader(unittest.TestLoader):
        def _get_module_from_name(self, name: str):
            try: return super()._get_module_from_name(name)
            except ImportError as e:
                if e.name in bgez.bge_modules:
                    raise unittest.SkipTest(e)
                raise e

    loader = CustomLoader()
    return loader.discover(os.path.dirname(bgez.path), '*_test*.py')

def run():
    runner = unittest.TextTestRunner(verbosity=2)
    results = runner.run(find())

    if not results.wasSuccessful():
        return 1
    return 0

if __name__ == '__main__':
    exit(run())
