import unittest

from bgez.core.utils.symbol import Symbol

class SymbolTest(unittest.TestCase):

    def test_unique(self):
        a = Symbol()
        b = Symbol()

        self.assertFalse(a == b)
        self.assertFalse(a is b)

    def test_targets(self):
        target = 'test'
        a = Symbol(target)
        b = Symbol(target)

        self.assertFalse(a == b)
        self.assertFalse(a is b)

    def test_collision(self):
        a = Symbol('a')

        self.assertFalse(a == TestNamespace.a)
        self.assertFalse(a is TestNamespace.a)

class TestNamespace:
    a = Symbol('a')
    b = Symbol('b')
