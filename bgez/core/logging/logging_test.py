from os.path import join, isfile, abspath
from collections import OrderedDict
from tempfile import gettempdir
from unittest import TestCase

from bgez.core.utils.time import loopUntil
from bgez.core.logging import BaseLogger
from bgez import logger

from .log import get_logger

PAYLOAD = "unepizzaquatrefromages"
LOGS_PATH = abspath(join(gettempdir(), "test_logs", "test.log"))

class LoggerTest(TestCase):

    def setUp(self):
        self.logger = get_logger("test")
        self.logger.setLevel(10)
        self.file_handler = self.logger.add_server(LOGS_PATH).handler

    def test_log(self):
        sub_logger = get_logger("test.test2.test3")

        log_level_method = OrderedDict(
            DEBUG = self.logger.debug,
            INFO = self.logger.info,
            WARNING = sub_logger.warning,
            ERROR = self.logger.error,
            CRITICAL = sub_logger.critical
        )
        remaining_lines = len(log_level_method)

        for _ in loopUntil(2):
            if isfile(LOGS_PATH):
                break
        else:
            return self.fail()

        self.file_handler.setLevel(10)
        for level, method in log_level_method.items():
            method(PAYLOAD) if level != "WARNING" else method(PAYLOAD)

        read_logs = []
        with open(LOGS_PATH, "r") as logfile:
            for _ in loopUntil(2):
                read = logfile.readline()
                if read:
                    read_logs.append(read)
                    remaining_lines -= 1
                    if remaining_lines == 0:
                        break
            else:
                return self.fail()

        for index, severity in enumerate(log_level_method):
            self._check_log(read_logs[index], severity)

    def test_no_duplicate(self):
        new_logger = get_logger("test")
        new_logger.add_server(LOGS_PATH)
        self.assertIs(new_logger, self.logger)
        self.assertIsNot(self.logger, logger)
        self.assertIsNot(new_logger, logger)

    def test_get_child(self):
        logger: BaseLogger = get_logger('sweet')
        child = logger.child('potato')
        self.assertEqual(child.name, 'sweet.potato')

    def _check_log(self, message, severity):
        self.assertIn(severity, message)
        self.assertIn(PAYLOAD, message)
