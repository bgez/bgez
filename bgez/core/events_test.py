import unittest

from bgez.core.events import Subject, EventEmitter

class SubjectTest(unittest.TestCase):

    def setUp(self):
        self.subject = Subject()

    def test_subscription(self):
        initial = 1
        value = initial
        increment = 3

        def callback(i):
            nonlocal value
            value += i

        self.subject.subscribe(callback)
        self.subject.notify(increment)
        self.assertEqual(value, initial + increment)

    def test_contains(self):
        subject = self.subject

        def a(): ...
        def b(): ...

        subject.subscribe(a)

        self.assertTrue(a in subject)
        self.assertFalse(b in subject)

    def test_subscribers(self):
        subject = self.subject

        def a(): ...
        def b(): ...

        subject.subscribe(a)

        self.assertTrue(a in subject.subscribers)
        self.assertFalse(b in subject.subscribers)

    def test_remove_while_emitting(self):
        subject = self.subject

        counter = 0

        def factory():
            def callback():
                nonlocal counter
                subject.unsubscribe(callback)
                subject.notify() # recursion
                counter += 1
            return callback

        for _ in range(5):
            subject.subscribe(factory())

        subject.notify()

        # test if the notification recursion worked correctly
        self.assertEqual(counter, 5 + 4 + 3 + 2 + 1)

class EventEmitterTest(unittest.TestCase):

    def setUp(self):
        self.emitter = EventEmitter()

    def _clean(self, event):
        self.emitter.subject(event)._clean()

    def test_trigger(self):
        event = 'test_trigger'
        value = 2

        called = False

        def callback(x):
            nonlocal called, value
            self.assertEqual(x, value)
            called = True

        self.emitter.on(event, callback)
        self.emitter.on(event, lambda x: ...)
        self.emitter.on(event, lambda x: ...)

        self.assertFalse(called)
        i = self.emitter.emit(event, value)
        self.assertTrue(called)
        self.assertEqual(i, 3)

    def test_unregistering(self):
        emitter = self.emitter
        event = 'test_unregistering'
        iterations = 4

        called = False

        def callback(x):
            ...

        self.assertEqual(0, len(emitter.subject(event)))
        for _ in range(iterations):
            emitter.on(event, callback)
        self.assertEqual(1, len(emitter.subject(event)))
        emitter.remove(event, callback)
        self.assertEqual(0, len(emitter.subject(event)))
        emitter.remove(event, callback)
        self.assertEqual(0, len(emitter.subject(event)))

    def test_events(self):
        emitter = self.emitter
        self.assertTupleEqual(emitter.events, ())

        emitter.once('a', lambda: ...)
        self.assertTupleEqual(emitter.events, ('a',))

        emitter.emit('a')
        self.assertTupleEqual(emitter.events, ('a',))

    def test_preset_events(self):
        events = 1, 2, 3
        emitter = EventEmitter(events)
        self.assertTupleEqual(emitter.events, events)

    def test_raise(self):
        event = 'test_raise'

        i = 0
        called = False

        def callback():
            nonlocal called, i
            self.assertEqual(i, 1)
            called = True

        def raiser():
            nonlocal i
            self.assertEqual(i, 0)
            i += 1
            raise Exception

        self.emitter.on(event, raiser)
        self.emitter.on(event, callback)

        self.emitter.emit(event)

        self.assertTrue(called)

    def test_decorator(self):
        event = 'test_decorator'
        value = 5

        called = False

        @self.emitter.on(event)
        def callback(x):
            nonlocal called
            self.assertEqual(x, value)
            called = True

        self.emitter.emit(event, value)
        self.assertTrue(called)

    def test_once(self):
        event = 'test_once'
        iterations = 5

        count = 0

        def factory():
            def callback():
                nonlocal count
                count += 1
            return callback

        for i in range(iterations):
            self.emitter.once(event, factory())

        for i in range(iterations * 10):
            self.emitter.emit(event)

        self.assertEqual(count, iterations)
        self.assertEqual(0, len(self.emitter.subject(event)))

    def test_subject_constraint(self):
        emitter = EventEmitter(('a', 'b'))

        emitter.on('a', lambda: self.fail())
        emitter.emit('b')

    def test_subject_constraint_failure(self):
        subjects = 'connect', 'disconnect', 'close'
        emitter = EventEmitter(subjects)
        typos = 'conect', 'd', 'cloe'

        for typo in typos:
            self.assertRaises(AttributeError, emitter.on, typo)

    def test_any(self):
        emitter = self.emitter
        subject = 'test'

        called = False

        @emitter.any
        def callback(event, *args):
            nonlocal called
            self.assertTupleEqual(args, ())
            self.assertEqual(event, subject)
            called = True

        emitter.emit(subject)
        self.assertTrue(called)
