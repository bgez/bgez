def undefined():
    None
undefined_method = \
    staticmethod(undefined)

from .singleton import *
from .logging import *

from .callbacks import *
from .toggle import *
from .pyon import *

from .saveable import *
from .dictable import *
