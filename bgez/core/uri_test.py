import unittest

from bgez.core.uri import URI, Query

class URITest(unittest.TestCase):

    def test_uri_parse(self):
        url = 'http://localhost:80/a/b?c=1#2'
        uri = URI(url)

        self.assertEqual(url, uri.s)
        self.assertEqual(uri.scheme, 'http')
        self.assertEqual(uri.authority, 'localhost:80')
        self.assertEqual(uri.path, '/a/b')
        self.assertEqual(uri.query, 'c=1')
        self.assertEqual(uri.fragment, '2')

    def test_uri_copy(self):
        uri = URI()

        authority = 'localhost'
        scheme = 'file'
        path = 'path'

        a = uri.copy(scheme=scheme)
        b = uri.copy(authority=authority)
        c = b.copy(path=path)

        self.assertEqual(a.scheme, scheme)

        self.assertEqual(b.scheme, '')
        self.assertEqual(b.authority, authority)

        self.assertEqual(c.scheme, '')
        self.assertEqual(c.authority, authority)
        self.assertEqual(c.path, path)

class QueryTest(unittest.TestCase):

    def test_uri_query(self):
        query = Query('a=1&b=2&a=3&c=é&z=')

        self.assertEqual(['1', '3'], query.getall('a'))
        self.assertEqual(1, query.get('a', type=int))
        self.assertIsNone(query.get('z'))

        self.assertIsNone(query.get('d'))
        query.add('d', 5)
        self.assertEqual(5, query.get('d'))
