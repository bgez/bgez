import unittest

from itertools import combinations_with_replacement

from bgez.core.inputs import ButtonType, AxisType

RELEASED = ButtonType.RELEASED
RELEASING = ButtonType.RELEASING
PRESSED = ButtonType.PRESSED
PRESSING = ButtonType.PRESSING

class InputTest(unittest.TestCase):

    def combinations(self):
        return combinations_with_replacement(ButtonType.STATES, 2)

    def get_in_table(self, combination, table):
        a, b = combination
        try:
            return table[a, b]
        except KeyError:
            return table[b, a]

    def do_key_test(self, function, table):
        for a, b in self.combinations():
            expected = self.get_in_table((a, b), table)
            self.assertEqual(function((a, b)), expected)

    def test_key_union(self):
        self.do_key_test(ButtonType._union, union_table)

    def test_key_intersection(self):
        self.do_key_test(ButtonType._intersection, intersection_table)

    def test_axis_union(self):
        expected = -.7
        values = 0, 0, .1, .2, .3, .4, .5, expected
        self.assertEqual(AxisType._union(values), expected)

    def test_axis_intersection(self):
        expected = -.2
        values = -1, 1, .4, expected
        self.assertEqual(AxisType._intersection(values), expected)

union_table = {

    # RELEASED
    (RELEASED, RELEASED): RELEASED,
    (RELEASED, PRESSING): PRESSING,
    (RELEASED, PRESSED): PRESSED,
    (RELEASED, RELEASING): RELEASING,

    # PRESSING
    (PRESSING, PRESSING): PRESSING,
    (PRESSING, PRESSED): PRESSED,
    (PRESSING, RELEASING): PRESSING,

    # PRESSED
    (PRESSED, PRESSED): PRESSED,
    (PRESSED, RELEASING): PRESSED,

    # RELEASING
    (RELEASING, RELEASING): RELEASING,
}

intersection_table = {

    # RELEASED
    (RELEASED, RELEASED): RELEASED,
    (RELEASED, PRESSING): RELEASED,
    (RELEASED, PRESSED): RELEASED,
    (RELEASED, RELEASING): RELEASED,

    # PRESSING
    (PRESSING, PRESSING): PRESSING,
    (PRESSING, PRESSED): PRESSING,
    (PRESSING, RELEASING): RELEASING,

    # PRESSED
    (PRESSED, PRESSED): PRESSED,
    (PRESSED, RELEASING): RELEASING,

    # RELEASING
    (RELEASING, RELEASING): RELEASING,
}
