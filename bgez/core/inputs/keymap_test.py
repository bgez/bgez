import unittest

from io import StringIO

from bgez.core.inputs import Keymap, InputType, RawInput

class IntegerType(InputType[int]):

    @classmethod
    def _normalize(cls, values):
        return values

    @classmethod
    def _union(cls, values):
        return max(values)

    @classmethod
    def _intersection(cls, values):
        return min(values)

class BooleanType(InputType[bool]):

    @classmethod
    def _normalize(cls, values):
        return values

    @classmethod
    def _union(cls, values):
        return any(values)

    @classmethod
    def _intersection(cls, values):
        return all(values)

class InputMock(RawInput):

    def __init__(self, value):
        self._value = value

    def value(self):
        return self._value

class IntegerInput(InputMock):
    type = IntegerType

class BooleanInput(InputMock):
    type = BooleanType

class KeymapTest(unittest.TestCase):

    REGISTRY = dict(
        a = IntegerInput(0),
        b = IntegerInput(1),
        c = IntegerInput(2),
        d = BooleanInput(True),
        e = BooleanInput(False),
    )

    def setUp(self):
        self.keymap = km = Keymap(self.REGISTRY)

        # ACTION A
        self.a = km.define('action.a', IntegerType, converters = {
            BooleanType: lambda input: 4 if input.value() else 2
        }, default=['a    '])

        # ACTION B
        self.b = km.define('action.b', BooleanType, converters = {
            IntegerType: lambda input: bool(input.value())
        }, default=['   b  + c', 'a'])

        # ACTION C
        self.c = km.define('action.c', BooleanType)

        self.assertEqual(len(self.a), 1)
        self.assertEqual(len(self.b), 2)
        self.assertEqual(len(self.c), 0)

    def test_type(self):
        km = self.keymap

        self.assertEqual(km.typeof('action.a'), IntegerType)
        self.assertEqual(km.typeof('action.b'), BooleanType)
        self.assertEqual(km.typeof('action.c'), BooleanType)

    def test_value(self):
        km = self.keymap

        self.assertEqual(km.valueof('action.a'), 0)
        self.assertEqual(km.valueof('action.b'), True)

        km.get('action.a').add('e')
        self.assertEqual(km.get('action.a').value(), 2)

    def test_get(self):
        km = self.keymap

        self.assertEqual(self.a, km.get('action.a'))
        self.assertEqual(self.b, km.get('action.b'))
        self.assertEqual(self.c, km.get('action.c'))

    def test_add_failure(self):
        km = self.keymap

        self.assertRaises(TypeError, km.add, 'action.c', 'a')

    def test_add(self):
        km = self.keymap

        km.add('action.a', 'e')
        self.assertTupleEqual(km.get('action.a').bindings, ('a', 'e'))

    def test_remove(self):
        km = self.keymap

        self.assertEqual(len(self.a), 1)
        km.remove('action.a', 'a ')
        self.assertEqual(len(self.a), 0)

    def test_sub_action(self):
        km = self.keymap

        sub = km.define('action.a.sub', BooleanType, default=['d'])
        self.assertTupleEqual(self.a.links, ())
        self.assertEqual(self.a.value(), 0)
        self.assertEqual(sub.value(), True)

        @km.link(self.a, sub)
        def test_linker(action):
            return 4 if action.value() else 2

        self.assertTupleEqual(self.a.links, ('action.a.sub',))
        self.assertEqual(self.a.value(), 4)
        self.assertEqual(sub.value(), True)

    def test_load(self):
        km = self.keymap

        def mockOpener(path, mode):
            if mode == 'r':
                return StringIO(TEST_KEYMAP_CONTENT)
            if mode == 'w':
                return StringIO()
            raise ValueError(f'unknown mode: "{mode}"')

        km.load(None, opener=mockOpener)

        self.assertTupleEqual(self.a.bindings, ('a',))
        self.assertTupleEqual(self.b.bindings, ('b', 'a+b'))
        self.assertTupleEqual(self.c.bindings, ('e',))

TEST_KEYMAP_CONTENT = '''\
[actions]
"action.b" = [ "b", "a + b",]
"action.c" = [ "e",]
'''
