from unittest import TestCase

from bgez.network import UDPPoint, UDPServer, UDPClient
from bgez.network import ChannelState, NetworkManager
from bgez.core.utils.time import loopUntil

srv_host = ("127.0.0.1", 30033)
payload = b"unepizzaquatrefromages"

class UDPTest(TestCase):

    def setUp(self):
        self.net_select = NetworkManager()
        self.srv = UDPServer(self.net_select, srv_host)
        self.channels = []
        self.srv.start()

    def tearDown(self):
        channels = self.srv.channels + self.channels
        self.srv.close()
        self.assertTrue(self.srv.closed)
        for channel in channels:
            self.assertEqual(channel.state, ChannelState.CLOSED)

    def test_client_server(self):
        self.received = 0
        clt, srv = UDPClient(self.net_select), self.srv
        clt.on("connect", self._on_connect)
        srv.on("connect", self._on_connect)

        clt.connect(srv_host)
        for _ in loopUntil(1):
            self.net_select()
            if self.received == 2:
                break
        self.assertEqual(self.received, 2)
        self.channels.append(clt)
        clt.close()

    def test_point_server(self):
        self.received = 0
        point, srv = UDPPoint(self.net_select), self.srv
        channel = point.get_channel(srv_host)
        srv.on("connect", self._on_connect)
        channel.on("read", self._on_read)

        sent = channel.send(payload)
        self.assertEqual(sent, len(payload))

        for _ in loopUntil(1):
            self.net_select()
            if self.received == 2:
                break
        self.assertEqual(self.received, 2)
        self.channels.append(channel)
        channel.close()

    def test_no_com(self):
        self.received = 0
        clt, point = _UDPClientTest(self.net_select), _UDPPointTest(self.net_select)
        channel = point.get_channel(clt.host)
        channel.on("read", self._on_read)
        clt.on("read", self._on_read)
        # The client needs to think that it's connected to try and receive messages
        clt.connect(("1.2.3.4", 666))

        sender1, sender2 = UDPClient(self.net_select), UDPClient(self.net_select)
        sender1.connect(("127.0.0.1", clt.host[1]))
        sender2.connect(("127.0.0.1", channel.host[1]))
        sender1.send(payload)
        sender2.send(payload)

        for _ in loopUntil(1):
            self.net_select()
            if clt.received == 1 and point.received == 1:
                break
        self.assertEqual(self.received, 0)

    def test_close_client(self):
        clt = UDPClient(self.net_select)
        clt.connect(("127.0.0.1", 30030))
        sent = clt.send(payload)
        self.assertEqual(sent, len(payload))
        clt.close()
        sent = clt.send(payload)
        self.assertEqual(sent, 0)

    def _on_read(self, channel, message):
        self.received += 1
        self.assertEqual(message, payload)

    def _on_connect(self, channel):
        channel.on("read", self._on_read)
        sent = channel.send(payload)
        self.assertEqual(sent, len(payload))

class _UDPClientTest(UDPClient):

    def __init__(self, *args, **kargs):
        super().__init__(*args, **kargs)
        self.received = 0

    def _on_message(self, *args, **kargs):
        super()._on_message()
        self.received += 1

class _UDPPointTest(UDPPoint):

    def __init__(self, *args, **kargs):
        super().__init__(*args, **kargs)
        self.received = 0

    def _on_message(self, *args, **kargs):
        super()._on_message()
        self.received += 1
