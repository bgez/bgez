from unittest import TestCase

from bgez.network import TCPServer, TCPClient, NetworkManager
from bgez.core.utils.time import loopUntil

srv_host = ("127.0.0.1", 30033)
payload = b"unepizzaquatrefromages"

class TCPServerTest(TestCase):

    def setUp(self):
        self.net_select = NetworkManager()

    def tearDown(self):
        self.clt.close()
        self.srv.close()
        self.assertTrue(self.srv.closed)

    def test_message(self):
        self._init_message()
        self._core()

    def test_stream(self):
        self._init_stream()
        self._core()

    def _core(self):
        received = 0

        def on_read(channel, payload):
            nonlocal received
            self.assertEqual(payload, payload)
            received += 1

        def on_connect(channel):
            channel.on("read", on_read)
            channel.send(payload)
            channel.send(payload)

        self.srv.on("connect", on_connect)
        self.clt.on("connect", on_connect)

        self.clt.connect(srv_host)
        self.assertEqual(self.clt.destination, srv_host)

        for _ in loopUntil(1):
            self.net_select()
            if received == self.expected:
                break
        else:
            self.fail()
        self.assertEqual(received, self.expected)

    def _init_stream(self):
        self.clt = TCPClient(self.net_select)
        self.srv = TCPServer(self.net_select, srv_host)
        self.srv.start()
        self.expected = 2

    def _init_message(self):
        self.clt = TCPClient(self.net_select, is_stream=False)
        self.srv = TCPServer(self.net_select, srv_host, is_stream=False)
        self.srv.start()
        self.expected = 4
