from unittest import TestCase

from bgez.network import UDPServer, UDPClient, TCPServer, TCPClient
from bgez.network import MultiplexedChannel, MultiplexedServer
from bgez.network import NetworkManager

from bgez.core.utils.time import loopUntil

payloads = {
    "pizza" : b"unepizzaquatrefromages",
    "poutine" : b"unepoutinelarge"
}
srv_host = ("127.0.0.1", 30033)

class MultiplexerTest(TestCase):

    def setUp(self):
        self.net_select = NetworkManager()
        self.subchannels = []
        self.connected = False

    def tearDown(self):
        self.srv.close()
        self.clt.close()

    def test_multiplexer_udp(self):
        self._init_udp()
        self._core()

    def test_multiplexer_tcp(self):
        self._init_tcp()
        self._core()

    def _core(self):
        clt, srv = self.clt, self.srv
        received = 0

        def on_read(sub, message):
            nonlocal received
            self.assertEqual(message, payloads[sub.get_id()])
            received += 1

        def on_subchannel(sub):
            sub.on("read", on_read)
            self.subchannels.append(sub)
            sub.send(payloads[sub.get_id()])

        def on_channel(channel):
            channel.on("subchannel", on_subchannel)

        srv.on("connect", on_channel)
        clt.on("connect", self._connected)
        srv.start()
        clt.connect(srv_host)

        for _ in loopUntil(1):
            self.net_select()
            if self.connected: break
        else: self.fail()

        for sub_id, payload in payloads.items():
            sub = clt.get_subchannel(sub_id)
            sub.on("read", on_read)
            sent = sub.send(payload)
            self.subchannels.append(sub)

        for _ in loopUntil(1):
            self.net_select()
            if received == 4:
                break

        self.assertEqual(received, 4)
        self.assertEqual(len(self.subchannels), 4)

        clt.close()
        srv.close()
        for sub in self.subchannels:
            sent = sub.send(payloads[sub.get_id()])
            self.assertEqual(sent, 0)

    def _init_udp(self):
        client  = UDPClient(self.net_select)
        server = UDPServer(self.net_select, srv_host)
        self.clt = MultiplexedChannel(client)
        self.srv = MultiplexedServer(server)

    def _init_tcp(self):
        client = TCPClient(self.net_select, is_stream=False)
        server = TCPServer(self.net_select, srv_host, is_stream=False)
        self.clt = MultiplexedChannel(client)
        self.srv = MultiplexedServer(server)

    def _connected(self, channel):
        self.connected = True
