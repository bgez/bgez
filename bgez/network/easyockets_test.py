from selectors import DefaultSelector, EVENT_READ
from bgez.core.utils.time import loopUntil
from unittest import TestCase

from bgez.network.easysocket import TCPSocket, UDPSocket, create_socket, timeout

target = ("127.0.0.1", 30033)
auto_host = ("127.0.0.1", 0)
payload = "unepizzaquatrefromages"

class EasySocketTest(TestCase):

    def test_udp(self):
        self.received = None
        selector = DefaultSelector()
        self.target_sock = UDPSocket(host=target, encoding="utf-8")
        self.sender_sock = UDPSocket(host=auto_host, encoding="utf-8")

        def on_read():
            self.received, sender = self.target_sock.recvfrom()
            self.assertEqual(sender, self.sender_sock.getsockname())

        selector.register(self.target_sock, EVENT_READ, on_read)
        sent = self.sender_sock.sendto(payload, target)
        self.assertEqual(len(payload), sent)

        for _ in loopUntil(1):
            res = selector.select(0)
            for key, event in res:
                key.data()
            if self.received is not None:
                break

        self.assertEqual(self.received, payload)
        selector.unregister(self.target_sock)
        self.sender_sock.close()
        self.target_sock.close()
        selector.close()

    def test_tcp(self):
        server_sock = TCPSocket(host=target, encoding="utf-8")
        server_sock.listen(10)
        self.selector = DefaultSelector()
        self.selector.register(server_sock, EVENT_READ, self._accept)
        self.received = self.new_conn = None

        client_sock = TCPSocket(host=auto_host, encoding="utf-8")
        client_sock.settimeout(0.1)
        try:
            client_sock.connect(target)
        except timeout:
            self.fail()
        else:
            sent = client_sock.send(payload)
            for _ in loopUntil(1):
                res = self.selector.select(0)
                for key, event in res:
                    key.data(key.fileobj)
                if self.received is not None:
                    break

            self.assertEqual(len(payload), sent)
            self.assertEqual(self.received, payload)
            self.selector.unregister(self.new_conn)
            self.new_conn.close()
        finally:
            client_sock.close()
            self.selector.unregister(server_sock)
            server_sock.close()
            self.selector.close()

    def _accept(self, sock):
        self.new_conn, _ = sock.accept()
        self.new_conn.encoding = "utf-8"
        self.selector.register(self.new_conn, EVENT_READ, self._read)

    def _read(self, conn):
        self.received = conn.recv()
